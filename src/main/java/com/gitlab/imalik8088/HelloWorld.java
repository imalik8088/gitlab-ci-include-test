package com.gitlab.imalik8088;

public class HelloWorld {

    static String helloMessage(String[] args) {
        return ("Hello " + String.join("-", args));
    }
}
